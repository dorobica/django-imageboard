from django.core.management.base import BaseCommand, CommandError
from django.utils.dateformat import format
import time

from apps.imageboard.models import Post

class Command(BaseCommand):

	def handle(self, *args, **kwargs):
		threads = Post.objects.filter(parent=None).order_by('id')
		for thread in threads:
			last_reply = thread.datetime_create
			if thread.replies.count() > 0:
				last = thread.replies.last()
				last_reply = last.datetime_create
			last_reply = float(format(last_reply, 'U'))
			now = time.time()

			diff = now - last_reply

			if diff > 14400:
				replies = thread.replies.all()
				for reply in replies:
					reply.delete()
				thread.delete()
				self.stdout.write("removed thread %s" % (thread))
			