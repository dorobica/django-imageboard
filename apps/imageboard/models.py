import os
import uuid
from django.db import models

from django.db.models.signals import post_delete
from django.dispatch.dispatcher import receiver

from sorl.thumbnail import ImageField

def upload_to(instance, filename):
	fn, fe = os.path.splitext(filename)
	
	return '/'.join(['images', str(uuid.uuid1())+fe])

class Board(models.Model):
	name = models.CharField(max_length=255)
	slug = models.SlugField(max_length=255, unique=True)
	datetime_create = models.DateTimeField(auto_now_add=True)
	datetime_update = models.DateTimeField(auto_now=True, auto_now_add=True)	

	def __str__(self):
		return self.name

class Post(models.Model):
	board = models.ForeignKey('Board', default=0)
	parent = models.ForeignKey('self', null=True, blank=True, related_name='replies')
	name = models.CharField(max_length=255, blank=True)
	comment = models.TextField(max_length=1000, blank=False)
	image = ImageField(upload_to=upload_to, blank=True)
	datetime_create = models.DateTimeField(auto_now_add=True)
	datetime_update = models.DateTimeField(auto_now=True, auto_now_add=True)

	def __str__(self):
		return self.name or "Anonymous"


@receiver(post_delete, sender=Post)
def post_delete(sender, instance, **kwargs):
	instance.image.delete(False)