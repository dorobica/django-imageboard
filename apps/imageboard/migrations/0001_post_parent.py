# encoding: utf8
from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('imageboard', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='parent',
            field=models.ForeignKey(to_field=u'id', blank=True, to='imageboard.Post', null=True),
            preserve_default=True,
        ),
    ]
