# encoding: utf8
from django.db import models, migrations
import sorl.thumbnail.fields
import apps.imageboard.models


class Migration(migrations.Migration):

    dependencies = [
        ('imageboard', '0001_post_parent'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='image',
            field=sorl.thumbnail.fields.ImageField(upload_to=apps.imageboard.models.upload_to, blank=True),
        ),
    ]
