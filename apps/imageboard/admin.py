from django.contrib import admin
from apps.imageboard.models import Post, Board

class BoardAdmin(admin.ModelAdmin):
	pass

class PostAdmin(admin.ModelAdmin):
	pass

admin.site.register(Post, PostAdmin)
admin.site.register(Board, BoardAdmin)