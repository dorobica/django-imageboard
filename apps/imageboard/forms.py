from django import forms

from models import Post

class PostForm(forms.ModelForm):
	parent_id = forms.IntegerField()
	board_id = forms.IntegerField()
	class Meta:
		model = Post
		fields = ['name', 'comment', 'image',]
